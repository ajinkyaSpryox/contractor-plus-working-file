//
//  Webservice.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 05/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
import SwiftyJSON

class Webservices: NSObject {
    

    static let MainBaseUrl = "http://contractorplus.dev.ganniti.com/api/"
    static let register = MainBaseUrl + "register"
    static let verify_otp = MainBaseUrl + "verify-otp"
    static let send_otp = MainBaseUrl + "send-otp"
    static let login = MainBaseUrl + "login"
    static let setMobileNumber = MainBaseUrl + "set-mobile-no"
    
    static let social_login = MainBaseUrl + "social-login"
    
    static let forget_password_verify_otp = MainBaseUrl + "forget-password-verify-otp"
    static let add_client = MainBaseUrl + "add-client"
    static let list_countries = MainBaseUrl + "list-countries"
    static let list_states = MainBaseUrl + "list-states"
    static let list_client_for_estimate = MainBaseUrl + "list-client-for-estimate"
    static let delete_client = MainBaseUrl + "delete-client"
    static let get_client_details = MainBaseUrl + "get-client-details"
    
    //MARK: URLS for Estimates
    static let list_estimate = MainBaseUrl + "list-estimates"
    static let create_estimate = MainBaseUrl + "create-estimate"
    static let get_estimate_details = MainBaseUrl + "get-estimate-details"
    
    //MARK: URLS FOR INVOICES
    static let list_invoice = MainBaseUrl + "list-invoices"
    static let create_invoice = MainBaseUrl + "create-invoice"
    static let delete_invoice = MainBaseUrl + "delete-invoice"
    static let invoice_details = MainBaseUrl + "invoice-details"
    
//  http://contractorplus.dev.ganniti.com/api/register
    
//print(MainBaseUrl)
    

}
public typealias APICallback = ( _ response: Any?, _ errorStr: String?) -> Void

class API {
    
    class func callAPI(_ urlStr: String,headers: [String:Any] ,params: [String:Any], showHud: Bool = true, completion: @escaping APICallback) {
        
        
                  if showHud{
                    
                       SVProgressHUD.show()
                       SVProgressHUD.setBackgroundColor(UIColor.white)
                   
                  }
        
        
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                manager.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default,headers: (headers as! HTTPHeaders)).responseJSON
                    { (response:DataResponse) in

                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            //print("Data: \(utf8Text)") // original server data as UTF8 string
                            if let newData = utf8Text.data(using: String.Encoding.utf8){
                                
                                do{
    
                                    let json = try JSON(data: newData)
                                    completion(json, nil)
    
                                   
                                }catch _ as NSError {
                                    // error
                                     completion(nil, "Error")
                                }
                            }
                            //    self.mainTblView.reloadData()
                        }
                        //   self.ProfileTblView.reloadData()
                }
        }

}



//API.getDirectData(Urls.HealthCard, params: dict, showHud: false) { (response, errorStr) in
//
//    guard errorStr == nil else {
//        SVProgressHUD.dismiss()
//        self.errorAlert(message: errorStr!)
//        return
//    }
//
//    if let str = response as? String {
//        print(str)
//        self.healthWebView.loadHTMLString(str, baseURL: nil)
//    }
//}
