//
//  Constant.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 05/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
import UIKit

let AppName = "Contractor Plus"
let appXApiKey = "WFIgqXp8Qyr0AF3wIVGglSKLN7qgw7EtPu5V7mWUbIaoSMGIUppTgaCKqaWh7Gb5Lyrf8L2A177"
let user_id = "user_id"
let isd_code = "isd_code"
let mobile_no = "mobile_no"
let token = "token"
let role = "role"
let is_otp_verified = "is_otp_verified"
let last_name = "last_name"
let first_name = "first_name"
let email = "email"
let full_name = "full_name"
let id = "id"
let NetworkError = "Internet connection might be offilne, Please try after some times."
let textfieldEmptyError = "Field's can't be empty."
let UIcolorRed : CGColor = UIColor(red:0.86, green:0.19, blue:0.19, alpha:1.0).cgColor //DC3131
let customeLightGrayColor = UIColor(red:0.85, green:0.86, blue:0.88, alpha:1.0) //DADBE0

let fcmTokenString = "fcmTokenString"
let isFacebookLogin = "isFacebookLogin"
let googleSignInClientId = "540286578116-2gj4fnjj6ts6qqmg52a1i5u7bh7jsv7p.apps.googleusercontent.com"

let formattedDateString = "dd-MM-yyyy"
let serverDateFormatString = "yyyy-MM-dd HH:mm:ss"

struct STORYBOARDNAMES {
    static let LOGIN_STORYBOARD = UIStoryboard(name: "LOGIN", bundle: nil)
    static let ESTIMATES_STORYBOARD = UIStoryboard(name: "Estimates", bundle: nil)
    static let INVOICES_STORYBOARD = UIStoryboard(name: "Invoices", bundle: nil)
    static let CLIENTS_STORYBOARD = UIStoryboard(name: "Clients", bundle: nil)
    static let SETTINGS_STORYBOARD = UIStoryboard(name: "Settings", bundle: nil)
}

enum SocialLogin: String {
    case FACEBOOK = "facebook"
    case GOOGLE = "google"
}


