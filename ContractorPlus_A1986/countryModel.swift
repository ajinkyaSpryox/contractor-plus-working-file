//
//  countryModel.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 13/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

class CountryModel{
   
    var id : String
    var countryName : String
    var isd_code : String
    
    init(id : String ,countryName: String ,isd_code :String){
        self.id = id
        self.countryName = countryName
        self.isd_code = isd_code
    }
}


class StateModel{
   
    var id : String
    var stateName : String
    var country_id : String
    init(id : String ,stateName: String ,country_id: String ){
        
        self.id = id
        self.stateName = stateName
        self.country_id = country_id
    }
}

