//
//  ClientModel.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 14/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
         
//{
//  "date" : "2019-12-16",
//  "client_details" : [
//    {
//      "number_of_estimate" : "12",
//      "email" : "g@g.com",
//      "mobile_no" : "54456465",
//      "amount_billed" : "1455",
//      "id" : 98,
//      "name" : "shuddh chhudha"
//    }
//  ]
//}

class ClientList{
    
    var data : String
    var clientList : [ClientListModel]
    
    init(data : String ,clientList : [ClientListModel] ) {
        self.data = data
        self.clientList = clientList
    }
    
}


class ClientListModel{
   
    var id : String
    var name : String
    var email : String
    var mobile_no : String
    var number_of_estimate : String
    var amount_billed : String
    
    init(id : String ,name : String ,email : String ,mobile_no : String ,number_of_estimate : String ,amount_billed : String){
        
        self.id = id
        self.name = name
        self.email = email
        self.mobile_no = mobile_no
        self.number_of_estimate = number_of_estimate
        self.amount_billed = amount_billed
        
    }
}
