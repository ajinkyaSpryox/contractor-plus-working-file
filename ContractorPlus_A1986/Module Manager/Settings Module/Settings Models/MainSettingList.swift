//
//  MainSettingList.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 14/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

struct MainSettingList {
    let displayIcon: String
    let title: String
    
}

var mainSettingListArr = [MainSettingList(displayIcon: "date1", title: "Estimates"),
                          MainSettingList(displayIcon: "date1", title: "Invoices"),
                          MainSettingList(displayIcon: "date1", title: "Payments"),
                          MainSettingList(displayIcon: "date1", title: "Branding"),
                          MainSettingList(displayIcon: "date1", title: "Quickbooks"),
                          MainSettingList(displayIcon: "date1", title: "Supplies"),
                          MainSettingList(displayIcon: "date1", title: "My Account"),
                          MainSettingList(displayIcon: "date1", title: "Time Clock"),
                          MainSettingList(displayIcon: "date1", title: "Mileage Log"),
                          MainSettingList(displayIcon: "date1", title: "Help"),
                          MainSettingList(displayIcon: "date1", title: "About"),
                          
                        ]
