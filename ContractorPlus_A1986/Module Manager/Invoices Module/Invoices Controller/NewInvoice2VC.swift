//
//  NewInvoice2VC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 03/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import Toast_Swift

class NewInvoice2VC: UIViewController {
    
    @IBOutlet weak var clientDropDownTextField: UITextField!
    @IBOutlet weak var invoiceDetailTextField: UITextField!
    @IBOutlet weak var invoiceDateTextField: UITextField!
    @IBOutlet weak var invoicePONumberTextField: UITextField!
    @IBOutlet weak var invoicePaymentTermsTextField: UITextField!
    @IBOutlet weak var addListItemTableView: UITableView!
    @IBOutlet weak var addListItemTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var subTotalAmountLbl: UILabel!
    @IBOutlet weak var addDiscountBtn: UIButton!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    // Received Variables
    var esitmateId: Int!
    var addLineItemsArray = [AddLineItem]()
    var addLineItemaArrayParam = [[String:Any]]()
    var createdInvoiceId = 0
    
    var invoiceToEditId = 0 //This comes from Manage invoices view controller
    
    //Variables
    var clientPickerData = [ClientEstimateDetail]()
    var invoiceParams = [String: Any]()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        title = "NEW INVOICE"
        
        addListItemTableView.delegate = self
        addListItemTableView.dataSource = self
        addListItemTableView.register(UINib(nibName: "AddLineItemTblCell", bundle: nil), forCellReuseIdentifier: "AddLineItemTblCell")
        
        clientDropDownTextField.delegate = self
        invoiceDateTextField.delegate = self
        
        invoiceDetailTextField.underlined()
        invoiceDateTextField.underlined()
        invoicePONumberTextField.underlined()
        invoicePaymentTermsTextField.underlined()
      
        serviceCalls()
        
    }
    
    fileprivate func setupInitialUIWith(invoiceToEdit: InvoiceDetailsData) {
        esitmateId = invoiceToEdit.estimateID
        clientDropDownTextField.text = invoiceToEdit.clientName
        invoiceDetailTextField.text = "\(invoiceToEdit.invoiceID)"
        invoiceDateTextField.text = convertServerDateToString(serverDateString: invoiceToEdit.dateOfInvoice)
        invoicePONumberTextField.text = invoiceToEdit.poNumber
        invoicePaymentTermsTextField.text = "\(invoiceToEdit.paymentTerms)"
        
        
        if invoiceToEdit.discountUnit == "percent" {
            addDiscountBtn.setTitle("\(invoiceToEdit.discount)%", for: .normal)
            calculateTotalPrice(with: Int(invoiceToEdit.lineItems.compactMap({$0.totalCount}).reduce(0, +)), and: invoiceToEdit.discount, isDiscountInPercent: true)
        }
        else {
            addDiscountBtn.setTitle("$\(invoiceToEdit.discount).00", for: .normal)
            calculateTotalPrice(with: Int(invoiceToEdit.lineItems.compactMap({$0.totalCount}).reduce(0, +)), and: invoiceToEdit.discount, isDiscountInPercent: false)
        }
        
        invoiceToEdit.lineItems.forEach { (item) in
            addLineItemsArray.append(item)
            addLineItemaArrayParam.append(item.dictionaryRepresentation)
        }
        
        
        
        addListItemTableView.reloadData()
        updateViewConstraints()
        subTotalAmountLbl.text = "$\(invoiceToEdit.lineItems.compactMap({$0.totalCount}).reduce(0, +)).00"
        invoiceParams["subtotal"] = invoiceToEdit.lineItems.compactMap({$0.totalCount}).reduce(0, +)
        invoiceParams["line_items"] = addLineItemaArrayParam
        invoiceParams["discount_unit"] = invoiceToEdit.discountUnit
        invoiceParams["discount"] = invoiceToEdit.discount
        invoiceParams["client_id"] = invoiceToEdit.clientID
        
    }
    
    fileprivate func serviceCalls() {
        
        if invoiceToEditId > 0 {
            getInvoiceDetails(with: invoiceToEditId)
        }
        else {
            getClientsListForEstimate()
            invoiceParams["discount_unit"] = "percent" //initail default values
            invoiceParams["discount"] = 0 //initial default values
        }
        
    }
    
    override func updateViewConstraints() {
        addListItemTableHeight.constant = addListItemTableView.contentSize.height
        super.updateViewConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
}

//MARK: IB ACTIONS IMPLEMENTATION
extension NewInvoice2VC {
    
    @IBAction func addLineItemBtnTapped(_ sender: UIButton) {
        //Present to Add Line Item popup view controller
        guard let addLinepopupVc = storyboard?.instantiateViewController(withIdentifier: "AddLineItemPopupVC") as? AddLineItemPopupVC else {return}
        addLinepopupVc.modalTransitionStyle = .crossDissolve
        addLinepopupVc.modalPresentationStyle = .overCurrentContext
        addLinepopupVc.delegate = self
        present(addLinepopupVc, animated: true, completion: nil)
    }
    
    @IBAction func addDiscountBtnTapped(_ sender: UIButton) {
        //Present to Add Discount Item popup view controller
        guard let addDiscountpopupVc = storyboard?.instantiateViewController(withIdentifier: "AddDiscountPopupVC") as? AddDiscountPopupVC else {return}
        addDiscountpopupVc.delegate = self
        present(addDiscountpopupVc, animated: true, completion: nil)
    }
    
    @IBAction func createEditInvoiceBtnTapped(_ sender: UIButton) {
        
        if createdInvoiceId > 0 {
            invoiceParams["invoice_id"] = createdInvoiceId //To Edit created invoice
            createInvoice()
            
        } else if invoiceToEditId > 0 {
            invoiceParams["invoice_id"] = invoiceToEditId //To Edit previously created invoice
            createInvoice()
        }
        else {
            invoiceParams["invoice_id"] = "" //To create new invoice
            createInvoice()
        }
        
        
    }
    
}



//MARK: TABLE VIEW DELEGATE DATA SOURCE IMPLEMENTATION
extension NewInvoice2VC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addLineItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddLineItemTblCell") as? AddLineItemTblCell else {return UITableViewCell()}
        
        cell.configureCell(itemDetails: addLineItemsArray[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: DATE PICKER FOR DATE TEXT FIELD Dalegate IMPLEMENTATION
extension NewInvoice2VC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == clientDropDownTextField {
            addPickerToClientTextField()
        }
        else if textField == invoiceDateTextField {
            addDatePickerTo(textField: textField)
        }
    }
    
    func addDatePickerTo(textField: UITextField) {
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        textField.inputView = datePicker
        
        datePicker.addTarget(self, action: #selector(selectedDateByUser(_:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc
    func selectedDateByUser(_ sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = formattedDateString
        invoiceDateTextField.text = formatter.string(from: sender.date)
    }
    
    func addPickerToClientTextField() {
        let clientIdPicker = UIPickerView()
        clientDropDownTextField.inputView = clientIdPicker
        clientIdPicker.delegate = self
        clientIdPicker.dataSource = self
    }
    
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
}

//MARK: UIPICKER VIEW DELEGATE IMPLEMENTATION
extension NewInvoice2VC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return clientPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return clientPickerData[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        clientDropDownTextField.text = clientPickerData[row].name
        invoiceParams["client_id"] = clientPickerData[row].id
    }
    
}

//MARK: ADD LINE ITEMS POPUP VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension NewInvoice2VC: AddLineItemPopupVCDelegate {
    
    func didSendAddLineItem(addLineItem: AddLineItem) {
        self.addLineItemsArray.append(addLineItem)
        self.addLineItemaArrayParam.append(addLineItem.dictionaryRepresentation)
        addListItemTableView.reloadData()
        updateViewConstraints()
        subTotalAmountLbl.text = "$\(addLineItemsArray.compactMap({$0.totalCount}).reduce(0, +)).00"
        invoiceParams["subtotal"] = addLineItemsArray.compactMap({$0.totalCount}).reduce(0, +)
        
        
        invoiceParams["line_items"] = addLineItemaArrayParam
        
        
        //Calculation for total Price
        if invoiceParams["discount_unit"] as! String == "percent" {
            calculateTotalPrice(with: addLineItemsArray.compactMap({$0.totalCount}).reduce(0, +), and: invoiceParams["discount"] as! Int, isDiscountInPercent: true)
        }
        else {
            calculateTotalPrice(with: addLineItemsArray.compactMap({$0.totalCount}).reduce(0, +), and: invoiceParams["discount"] as! Int, isDiscountInPercent: false)
        }
        
        
        
    }
    
}

//MARK: ADD DISCOUNT POPUP VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension NewInvoice2VC: AddDiscountPopupVCDelegate {
    
    func didselectDiscount(value: Int, unit: String) {
        
        invoiceParams["discount_unit"] = unit
        invoiceParams["discount"] = value
        
        if unit == "percent" {
            addDiscountBtn.setTitle("\(value)%", for: .normal)
            calculateTotalPrice(with: addLineItemsArray.compactMap({$0.totalCount}).reduce(0, +), and: invoiceParams["discount"] as! Int, isDiscountInPercent: true)
        }
        else {
            addDiscountBtn.setTitle("$\(value).00", for: .normal)
            calculateTotalPrice(with: addLineItemsArray.compactMap({$0.totalCount}).reduce(0, +), and: invoiceParams["discount"] as! Int, isDiscountInPercent: false)
        }
        
    }
    
}

extension NewInvoice2VC {
    
    func calculateTotalPrice(with subtotalPrice: Int, and discount: Int, isDiscountInPercent: Bool) {
        if isDiscountInPercent {
            //print("Calculation in percentage")
            let discountPrice = (discount * subtotalPrice) / 100
            totalAmountLbl.text = "$\(subtotalPrice - discountPrice).00"
        }
        else {
            //print("Calculate reducing the price")
            totalAmountLbl.text = "$\(subtotalPrice - discount).00"
        }
    }
}

//MARK: API SERVICES IMPLEMENTATIONS
extension NewInvoice2VC {
    
    //Todo: Get Clients Name in Drop Down
    func getClientsListForEstimate() {
        
        let params = ["page_number": 0]
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_client_for_estimate, method: .post, parameters: params, encodingType: URLEncoding.default, headers: headers) { [weak self] (clientList: EstimateClientsList!, errorMessage) in
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = clientList {
                    if returnedResponse.success == "true" {
                        //Success
                        
                        returnedResponse.data.clients.forEach { (client) in
                            client.clientEstimateDetails.forEach { (detail) in
                                self.clientPickerData.append(detail)
                            }
                        }
                        
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            
                        }
                    }
                }
                
            }
            
            
        }
        
    }
    
    func createInvoice() {
        
        invoiceParams["estimate_id"] = esitmateId
        invoiceParams["tax"] = 0 //default value
        invoiceParams["is_client_signature"] = true //default value
        invoiceParams["is_my_signature"] = true //default value
        invoiceParams["date_of_invoice"] = invoiceDateTextField.text
        invoiceParams["po_number"] = invoicePONumberTextField.text
        invoiceParams["payment_terms"] = invoicePaymentTermsTextField.text
        
        guard (invoiceParams["client_id"] != nil) else {
            view.makeToast("Client ID Required", duration: 2.0, position: .bottom)
            return
        }
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        //print(self.invoiceParams)
        
        GenericWebservice.instance.getServiceData(url: Webservices.create_invoice, method: .post, parameters: invoiceParams, encodingType: JSONEncoding.default, headers: headers) { [weak self] (createdInvoice: CreateInvoice!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if createdInvoice.success == "true" {
                    //Success
                    self.createdInvoiceId = createdInvoice.data.id
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.invoiceDetailTextField.text = "\(createdInvoice.data.id)"
                        self.view.makeToast(createdInvoice.message, duration: 2.0, position: .bottom)
                    }
                }
                else {
                    //Failure
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(createdInvoice.message, duration: 2.0, position: .bottom)
                        print(createdInvoice.message)
                    }
                }
            }
            
        }
        
    }
    
    func getInvoiceDetails(with invoiceId: Int) {
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let params = ["invoice_id" : invoiceId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.invoice_details, method: .post, parameters: params, encodingType: URLEncoding.default, headers: headers) { [weak self] (invoiceDetails: InvoiceDetails!, errorString) in
            
            guard let self = self else {return}
            
            if let error = errorString {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = invoiceDetails {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.setupInitialUIWith(invoiceToEdit: returnedResponse.data)
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message)
                        }
                    }
                }
                
            }
            
        }
        
    }
    
}
