//
//  ManageInvoicesVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 02/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ManageInvoicesVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var invoiceTableView: UITableView!
    @IBOutlet weak var emptyInvoiceStack: UIStackView!
    
    //Variables
    var invoicesDataArray = [InvoiceList]()
    
    //Pagination Variables
    var currentPageCount = 1
    var maxPageCount = 0
    var isLoadingMoreInvoices = false
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        title = "INVOICES"
        emptyInvoiceStack.isHidden = true
        invoiceTableView.delegate = self
        invoiceTableView.dataSource = self
        invoiceTableView.register(UINib(nibName: "InvoiceDetailsTblCell", bundle: nil), forCellReuseIdentifier: "InvoiceDetailsTblCell")
        invoiceTableView.register(UINib(nibName: "InvoiceHeaderTblCell", bundle: nil), forCellReuseIdentifier: "InvoiceHeaderTblCell")
    }
    
    fileprivate func serviceCalls() {
        getInvoiceListFor(pageNumber: 0)
    }
    
    //MARK: IB-ACTIONS
    @IBAction func createInvoiceBtnTapped(_ sender: UIButton) {
        //Go to New Invoice 1 View Controller
        guard let newInvoiceVC1 = storyboard?.instantiateViewController(withIdentifier: "NewInvoice1VC") as? NewInvoice1VC else {return}
        navigationController?.pushViewController(newInvoiceVC1, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ManageInvoicesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return invoicesDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoicesDataArray[section].invoices.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceHeaderTblCell") as? InvoiceHeaderTblCell else {return UIView()}
        
        cell.configureCell(invoiceList: invoicesDataArray[section])
        
        return cell.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceDetailsTblCell") as? InvoiceDetailsTblCell else {return UITableViewCell()}
        
        let invoicesData = invoicesDataArray[indexPath.section].invoices[indexPath.row]
        
        cell.configureCell(invoiceDetail: invoicesData, indexPath: indexPath, delegate: self)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if currentPageCount < maxPageCount && !isLoadingMoreInvoices {
            currentPageCount += 1
            getInvoiceListFor(pageNumber: currentPageCount)
        }
        
    }
    
}

//MARK: Invoice Detail Table Cell Delegate Implementation
extension ManageInvoicesVC: InvoiceDetailsTblCellDelegate {
    
    func didTapDeleteInvoiceBtn(invoiceDetails: Invoice, at indexPath: IndexPath) {
        
        invoicesDataArray[indexPath.section].invoices.remove(at: indexPath.row)
        let indexPath = IndexPath(item: indexPath.row, section: indexPath.section)
        invoiceTableView.deleteRows(at: [indexPath], with: .fade)
        deleteInvoice(for: invoiceDetails.id)
    }
    
    func didTapEditInvoiceBtn(invoiceDetails: Invoice, at indexPath: IndexPath) {
        guard let editInvoicevc = storyboard?.instantiateViewController(withIdentifier: "NewInvoice2VC") as? NewInvoice2VC else {return}
        editInvoicevc.invoiceToEditId = invoiceDetails.id
        navigationController?.pushViewController(editInvoicevc, animated: true)
    }
    
}

//MARK: API SERVICE CALLS IMPLEMENTATION
extension ManageInvoicesVC {
    
    //Get All Invoices
    func getInvoiceListFor(pageNumber: Int) {
        
        isLoadingMoreInvoices = true
        
        view.makeToastActivity(.center)
        
        let parameters = ["page_number": pageNumber]
        
        if pageNumber == 0 {
            invoicesDataArray.removeAll()
        }
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_invoice, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (invoices: ListInvoice!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
                
            }
            else {
                
                if let returnedResponse = invoices {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        if let data = returnedResponse.data {
                            
                            self.maxPageCount = data.pageCount
                            
                            returnedResponse.data?.invoiceList.forEach({ (invoice) in
                                self.invoicesDataArray.append(invoice)
                            })
                            
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.invoiceTableView.reloadData()
                            }
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                    
                }
                
            }
            self.isLoadingMoreInvoices = false
        }
        
    }
    
    func deleteInvoice(for id: Int) {
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let params = ["invoice_id" : id]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_invoice, method: .post, parameters: params, encodingType: URLEncoding.default, headers: headers) { [weak self] (deletedInvoice: DeleteInvoice!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = deletedInvoice {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                        self.getInvoiceListFor(pageNumber: self.currentPageCount)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
}
