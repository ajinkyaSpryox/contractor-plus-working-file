//
//  NewInvoice1VC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 03/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire

class NewInvoice1VC: UIViewController {
    
    @IBOutlet weak var estimateSelectionTextField: DropDown!
    
    //Variables
    var estimateNamesArray = [String]()
    var estimateIdArray = [Int]()
    var selectedEstimateId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
    }
    
    private func setupInitialUI() {
        serviceCalls()
        dropDownTextFieldActions()
        title = "NEW INVOICE"
    }
    
    private func serviceCalls() {
        getAllEstimateList()
    }
    
    //MARK: IB_ACTIONS
    @IBAction func createInvoiceBtnTapped(_ sender: UIButton) {
        //Go to New Invoice Controller 2
        guard let newInvoice2Vc = storyboard?.instantiateViewController(withIdentifier: "NewInvoice2VC") as? NewInvoice2VC else {return}
        newInvoice2Vc.esitmateId = selectedEstimateId
        
        if selectedEstimateId > 0 {
            navigationController?.pushViewController(newInvoice2Vc, animated: true)
        }else {
            view.makeToast("Kindly select an Estimate", duration: 2.0, position: .bottom)
        }
        
    }
    
}

//MARK: DROP DOWN TEXT FIELD ACTIONS
extension NewInvoice1VC {
    
    func dropDownTextFieldActions() {
        
        estimateSelectionTextField.didSelect { (selectedText, index, id) in
            self.estimateSelectionTextField.text = selectedText
            print("At Index: \(index)")
            print("And ID: \(id)")
            self.selectedEstimateId = id
        }
        
    }
    
}

//MARK: API SERVICE IMPLEMENTATIONS
extension NewInvoice1VC {
    
    func getAllEstimateList() {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters = ["page_number": 0]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (listEstimates: ListEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listEstimates {
                    
                    if returnedResponse.success == "true" {
                        //success
                        
                        if let data = returnedResponse.data {
                            for estimate in data.estimatesDetailData {
                                for detail in estimate.estimates {
                                    self.estimateNamesArray.append(detail.clientName)
                                    self.estimateIdArray.append(detail.id)
                                }
                            }
                            
                        }
                        
                        self.estimateSelectionTextField.optionArray = self.estimateNamesArray
                        self.estimateSelectionTextField.optionIds = self.estimateIdArray
                        //print(self.estimateNamesArray)
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
}
