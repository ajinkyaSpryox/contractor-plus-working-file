//
//  CreateInvoice.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 01/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - CreateInvoice
struct CreateInvoice: Codable {
    let success, message: String
    let data: CreatedInvoiceData
}

// MARK: - DataClass
struct CreatedInvoiceData: Codable {
    let id: Int
    let pdfLink: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case pdfLink = "pdf_link"
    }
}


//MARK: Delete Invoince
struct DeleteInvoice: Codable {
    let success, message: String
}
