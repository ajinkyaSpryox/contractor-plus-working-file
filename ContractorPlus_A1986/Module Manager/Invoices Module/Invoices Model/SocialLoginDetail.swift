//
//  SocialLoginDetail.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 10/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//


import Foundation

// MARK: - SocialLoginDetail
struct SocialLoginDetail: Codable {
    let success, message: String
    let data: SocialLoginDetailData
}

// MARK: - DataClass
struct SocialLoginDetailData: Codable {
    let token: String
    let userDetails: UserDetails
    let isOtpVerified: String
    let accessDetails: AccessDetails?
    let role: String

    enum CodingKeys: String, CodingKey {
        case token
        case userDetails = "user_details"
        case isOtpVerified = "is_otp_verified"
        case accessDetails = "access_details"
        case role
    }
}

// MARK: - AccessDetails
struct AccessDetails: Codable {
    let view: [String]
}

// MARK: - UserDetails
struct UserDetails: Codable {
    let id: Int
    let fullName, firstName, lastName, email: String
    let profilePicture: String
    let mobileNumber: String
    let isdCode: String

    enum CodingKeys: String, CodingKey {
        case id
        case fullName = "full_name"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case profilePicture = "profile_picture"
        case mobileNumber = "mobile_no"
        case isdCode = "isd_code"
        
    }
}
