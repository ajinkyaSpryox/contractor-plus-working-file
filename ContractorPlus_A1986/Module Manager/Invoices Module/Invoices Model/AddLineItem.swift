//
//  AddLineItems.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 04/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

struct AddLineItem: Codable {
    let id: Int
    let description: String
    let suppliesCount: Int
    let labourCount: Int
    
    var totalCount: Int {
        return suppliesCount + labourCount
    }
    
    var dictionaryRepresentation: [String: Any] {
        return [
            "id" : id,
            "description" : description,
            "labour_total" : labourCount,
            "supply_total" : suppliesCount
        ]
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case description = "description"
        case labourCount = "labour_total"
        case suppliesCount = "supply_total"
    }
}
