//
//  AddLineItemTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 04/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class AddLineItemTblCell: UITableViewCell {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(itemDetails: AddLineItem) {
        itemNameLabel.text = itemDetails.description
        itemPriceLabel.text = "$ \(itemDetails.totalCount)"
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        print("Close Button Tapped")
    }
    
}
