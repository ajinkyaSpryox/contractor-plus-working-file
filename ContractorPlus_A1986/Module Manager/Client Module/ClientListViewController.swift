

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import Photos
import CountryList
import iOSDropDown
import Toast_Swift

var client_id : String!
var from_edit = false
class ClientListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    var clientDetailsArr = [ClientEstimateDetailData]()
    
    @IBOutlet weak var plusButton: UIButton!
    
    
    @IBOutlet weak var clientListTblView: UITableView!
    @IBAction func plusbtnaction(_ sender: UIButton) {
    }
    
    @IBOutlet weak var recordUiView: UIView!
    @IBOutlet weak var noRecordUiView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        noRecordUiView.isHidden = true
        recordUiView.isHidden = false
        //client_list()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getClientList()
    }
    
   override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        let titleTextAttributed: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white, .font: UIFont(name: "NotoSans-Regular", size: 20) as Any]
        navigationController?.navigationBar.titleTextAttributes = titleTextAttributed
        
        plusButton.cornerRadius1(value: 30.0, shadowColor: UIcolorRed  , borderColor: UIColor.clear.cgColor)
        
        // Do any additional setup after loading the view.
    }
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return clientDetailsArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return clientDetailsArr[section].clientEstimateDetails.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! ClientListTableViewCell
        cell.mainUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        cell.countUiView.cornerRadius1(value: 20.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.black.cgColor)
//       cell.editUiView.addRightBorderWithColor(color: UIColor.black, width: 1.0)
        cell.deleteUiView.addLeftBorderWithColor(color:  UIColor.black, width: 1.0)
        cell.optionUIView.roundCorners([.bottomLeft, .bottomRight], radius: 12)
        
        cell.clientname.text = clientDetailsArr[indexPath.section].clientEstimateDetails[indexPath.row].name
        cell.contactNumberLbl.text = clientDetailsArr[indexPath.section].clientEstimateDetails[indexPath.row].mobileNo
        cell.emailIdlbl.text = clientDetailsArr[indexPath.section].clientEstimateDetails[indexPath.row].email
        cell.estimateval.text = clientDetailsArr[indexPath.section].clientEstimateDetails[indexPath.row].numberOfEstimate
        cell.amountBilledValLbl.text = clientDetailsArr[indexPath.section].clientEstimateDetails[indexPath.row].amountBilled
        
        let tapGestureRecognizer_delete = UITapGestureRecognizer(target: self, action: #selector(self.deleteClient(recognizer:)))
        cell.deleteUiView.tag = indexPath.section
        cell.deleteUiView.sub_tag = indexPath.row
        cell.deleteUiView.addGestureRecognizer(tapGestureRecognizer_delete)
        
        let tapGestureRecognizer_edit = UITapGestureRecognizer(target: self, action: #selector(self.editClient(recognizer:)))
        
        cell.editUiView.tag = indexPath.section
        cell.editUiView.sub_tag = indexPath.row
        
        cell.editUiView.addGestureRecognizer(tapGestureRecognizer_edit)
        
        return cell
    }
    @objc func deleteClient(recognizer : UITapGestureRecognizer)
    {
        
        print(recognizer.view!.tag)
        print(recognizer.view!.sub_tag)
        
        client_delete(client_id: "\(clientDetailsArr[recognizer.view!.tag].clientEstimateDetails[recognizer.view!.sub_tag].id)",section:recognizer.view!.tag,row:  recognizer.view!.sub_tag)
        
        
    }
    
    @objc func editClient(recognizer : UITapGestureRecognizer)
    {
        
           print(recognizer.view!.tag)
           print(recognizer.view!.sub_tag)
        client_id = "\(clientDetailsArr[recognizer.view!.tag].clientEstimateDetails[recognizer.view!.sub_tag].id)"
        from_edit = true
        self.performSegue(withIdentifier: "Add_client", sender: nil)
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 306
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! DateViewHeaderTableViewCell
        cell.dateLbl.text = clientDetailsArr[section].date
        return cell
    }

}
extension ClientListViewController {
    
    func getClientList() {
        
        self.view.makeToastActivity(.center)
        
        let parameters = ["page_number": 0]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_client_for_estimate, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [unowned self] (clientDetails: EstimateClientsList!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = clientDetails {
                    if returnedResponse.success == "true" {
                        //succes
                        self.clientDetailsArr = returnedResponse.data.clients
                        //print(self.clientDetailsArr)
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.clientListTblView.reloadData()
                        }
                        
                    }
                    else {
                        //failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
        }
        
    }
    
    func client_delete(client_id: String, section: Int , row: Int){
                    
        print(client_id)
        print(section)
        print(row)
        
                    let header = ["Accept" : "application/json",
                                  "x-api-key": appXApiKey,
                                "Authorization": "Bearer \(UserDefaults.standard.value(forKey: token)!)"]
                    
           
                    let parameter : [String : Any] = [
                                   
            //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
            //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
                        
                       "client_id": client_id,
    //                    "token": "Bearer \(UserDefaults.standard.value(forKey: token)!)",
                               
                    ]
                    
                  
                API.callAPI(Webservices.delete_client, headers: header, params: parameter) { (response, errorStr) in
                        
                        guard errorStr == nil else {
                            SVProgressHUD.dismiss()
                            self.errorAlert(message: errorStr!)
                            return
                        }
                        
    //                print(response)
                        if let json = response as? JSON {
                            
        //
                            SVProgressHUD.dismiss()
                           
            //                 let json = try JSON(data: newData)
                               let postdata = json["data"]
                               let Success = json["success"].boolValue
                               let Message = json["message"].stringValue
                               let eventsData = json["data"]["events"]
    //                        print(postdata.first)
                            
                            var countryDataArray = [String]()
                               if(Success == true){
//                                for j in 0..<postdata.count{
//                                    var clientListModelData = [ClientListModel]()
//                                    for k in 0..<postdata[j]["client_details"].count{
//
//                                        clientListModelData.append(ClientListModel.init(id: postdata[j]["client_details"][k]["id"].stringValue, name: postdata[j]["client_details"][k]["name"].stringValue, email: postdata[j]["client_details"][k]["email"].stringValue, mobile_no: postdata[j]["client_details"][k]["mobile_no"].stringValue, number_of_estimate: postdata[j]["client_details"][k]["number_of_estimate"].stringValue, amount_billed: postdata[j]["client_details"][k]["amount_billed"].stringValue))
//                                    }
//
//
//
//                                    print(postdata[j])
//                                    self.clientList.append(ClientList.init(data: postdata[j]["date"].stringValue, clientList: clientListModelData))
//                                }
                             
        //
                                self.successAlert(message: Message)
                                
                                self.clientDetailsArr[section].clientEstimateDetails.remove(at: row)
                                
                                self.clientListTblView.reloadData()
                               }else{
                                self.errorAlert(message: Message)
                            }
                            
                        }
                    }
                    
                }
}
