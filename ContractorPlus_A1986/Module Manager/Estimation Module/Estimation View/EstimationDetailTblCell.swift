//
//  EstimationDetailTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 25/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol EstimationDetailTblCellDelegate: class {
    func didTapEstimateEditBtn(estimateDetail: Estimate, at indexpath: IndexPath)
}

class EstimationDetailTblCell: UITableViewCell {
    
    @IBOutlet weak var bottomOptionsView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var estimateCountLabel: UILabel!
    @IBOutlet weak var amountBilledLabel: UILabel!
    
    weak var delegate: EstimationDetailTblCellDelegate?
    var selectedEstimate: Estimate!
    var selectedIndexpath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(estimateDetail: Estimate, delegate: EstimationDetailTblCellDelegate, indexpath: IndexPath) {
        
        self.selectedEstimate = estimateDetail
        self.selectedIndexpath = indexpath
        self.delegate = delegate
        
        nameLabel.text = estimateDetail.clientName
        
    }
    
    @IBAction func editEstimateBtnTapped(_ sender: UIButton) {
        delegate?.didTapEstimateEditBtn(estimateDetail: selectedEstimate, at: selectedIndexpath)
    }
    
}
