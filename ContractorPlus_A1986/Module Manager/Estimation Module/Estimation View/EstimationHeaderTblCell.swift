//
//  EstimationHeaderTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 31/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class EstimationHeaderTblCell: UITableViewCell {
    
    @IBOutlet weak var estimateCreationDateLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(estimateDate: ListEstimateDetailsData) {
        estimateCreationDateLbl.text = estimateDate.date
    }
    
}
