//
//  AddTaskTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 26/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class TaskDetailTblCell: UITableViewCell {
    
    @IBOutlet weak var contentHolderView: UIView!
    @IBOutlet weak var bottomOptionsView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var printButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentHolderView.clipsToBounds = true
        contentHolderView.layer.cornerRadius = 10
        contentHolderView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    
    
}
