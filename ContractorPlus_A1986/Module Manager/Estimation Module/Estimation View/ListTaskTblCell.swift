//
//  ListTaskTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class ListTaskTblCell: UITableViewCell {
    
    @IBOutlet weak var listTaskSuppliesTblView: UITableView!
    @IBOutlet weak var listTaskSuppliesTblHeight: NSLayoutConstraint!
    @IBOutlet weak var listTaskSuppliesPhotoCollectionView: UICollectionView!
    @IBOutlet weak var bottomOptionsView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        listTaskSuppliesTblView.delegate = self
        listTaskSuppliesTblView.dataSource = self
        listTaskSuppliesPhotoCollectionView.delegate = self
        listTaskSuppliesPhotoCollectionView.dataSource = self
        listTaskSuppliesTblView.register(UINib(nibName: "ListTaskSuppliesTblCell", bundle: nil), forCellReuseIdentifier: "ListTaskSuppliesTblCell")
        listTaskSuppliesPhotoCollectionView.register(UINib(nibName: "BeforePhotosCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BeforePhotosCollectionCell")
        
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        
    }
    
    override func updateConstraints() {
        listTaskSuppliesTblHeight.constant = listTaskSuppliesTblView.contentSize.height - 50
        super.updateConstraints()
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListTaskTblCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTaskSuppliesTblCell") as? ListTaskSuppliesTblCell else {return UITableViewCell()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: COLLECTION VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListTaskTblCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BeforePhotosCollectionCell", for: indexPath) as? BeforePhotosCollectionCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width
        let cellWidth = (width - 10) / 2
        let height = collectionView.bounds.height
        return CGSize(width: cellWidth, height: height)
    }
    
}
