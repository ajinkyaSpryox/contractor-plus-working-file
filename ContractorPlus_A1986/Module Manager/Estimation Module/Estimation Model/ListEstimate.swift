//
//  ListEstimate.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 31/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListEstimate
struct ListEstimate: Codable {
    let success, message: String
    let data: ListEstimateData?
}

// MARK: - DataClass
struct ListEstimateData: Codable {
    let estimatesDetailData: [ListEstimateDetailsData]
    let pageCount: Int
    
    enum CodingKeys: String, CodingKey {
        case estimatesDetailData =  "estimate_list"
        case pageCount = "page_count"
    }
}

// MARK: - Datum
struct ListEstimateDetailsData: Codable {
    let date: String
    let estimates: [Estimate]
}


 // MARK: - Estimate
 struct Estimate: Codable {
     let id: Int
     let clientName: String
     let clientAddress: String
     let status: Status
     
     enum CodingKeys: String, CodingKey {
         case id
         case clientName = "client_name"
         case clientAddress = "client_address"
         case status
     }
 }

enum Status: String, Codable {
    case pending = "pending"
}

