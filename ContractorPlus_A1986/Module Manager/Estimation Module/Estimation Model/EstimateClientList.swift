//
//  EstimateClientList.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 31/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - EstimateClientsList
struct EstimateClientsList: Codable {
    let success, message: String
    let data: EstimateClientsListData
}

// MARK: - DataClass
struct EstimateClientsListData: Codable {
    var clients: [ClientEstimateDetailData]
    let pageCount: Int
    
    enum CodingKeys: String, CodingKey {
        case clients = "client_list"
        case pageCount = "page_count"
    }
}

// MARK: - ClientEstimateDetailData
struct ClientEstimateDetailData: Codable {
    let date: String
    var clientEstimateDetails: [ClientEstimateDetail]
    
    enum CodingKeys: String, CodingKey {
        case date
        case clientEstimateDetails = "client_details"
    }
}

// MARK: - ClientEstimateDetail
struct ClientEstimateDetail: Codable {
    let id: Int
    let name, email, mobileNo, numberOfEstimate: String
    let amountBilled: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case mobileNo = "mobile_no"
        case numberOfEstimate = "number_of_estimate"
        case amountBilled = "amount_billed"
    }
    
    
}
