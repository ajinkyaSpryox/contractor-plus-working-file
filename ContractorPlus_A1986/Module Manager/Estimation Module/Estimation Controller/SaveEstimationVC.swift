//
//  SaveEstimationVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 26/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class SaveEstimationVC: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var taskTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        title = "New Estimate"
        taskTableView.register(UINib(nibName: "TaskDetailTblCell", bundle: nil), forCellReuseIdentifier: "TaskDetailTblCell")
        taskTableView.delegate = self
        taskTableView.dataSource = self
    }
    
    //IB-Actions
    @IBAction func saveEstimateBtnTapped(_ sender: UIButton) {
        guard let addTaskVc = storyboard?.instantiateViewController(withIdentifier: "AddTaskVC") as? AddTaskVC else {return}
        navigationController?.pushViewController(addTaskVc, animated: true)
    }
    
}

//MARK: Task Table View Implementation
extension SaveEstimationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TaskDetailTblCell") as? TaskDetailTblCell else {return UITableViewCell()}
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
