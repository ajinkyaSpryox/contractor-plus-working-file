//
//  AddNewAddressPopVC.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 15/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class AddNewAddressPopVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var address1TextView: UITextView!
    @IBOutlet weak var address2TextView: UITextView!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        address1TextView.delegate = self
        address2TextView.delegate = self
        
        address1TextView.text = "Enter Address"
        address1TextView.textColor = .lightGray
        
        address2TextView.text = "Enter Address"
        address2TextView.textColor = .lightGray
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension AddNewAddressPopVC {
    
    @IBAction func confirmAddressBtnTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension AddNewAddressPopVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter Address"
            address1TextView.textColor = .lightGray
        }
        
    }
    
}
