//
//  AddTaskVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 27/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class AddTaskVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var supplySearchTableView: UITableView!
    @IBOutlet weak var supplyDisplayTableView: UITableView!
    @IBOutlet weak var addSuppliesTextField: CustomInsetTextField!
    @IBOutlet weak var supplyDisplayTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var beforePhotosCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        self.title = "ADD TASK"
        supplyDisplayTableView.delegate = self
        supplyDisplayTableView.dataSource = self
        supplySearchTableView.delegate = self
        supplySearchTableView.dataSource = self
        beforePhotosCollectionView.dataSource = self
        beforePhotosCollectionView.delegate = self
        
        supplySearchTableView.isHidden = true
        
        customizeTableViewDesign(tableView: supplySearchTableView)
        supplyDisplayTableView.layer.cornerRadius = 8.0
        
        
        supplyDisplayTableView.register(UINib(nibName: "SupplyDisplayTblCell", bundle: nil), forCellReuseIdentifier: "SupplyDisplayTblCell")
        beforePhotosCollectionView.register(UINib(nibName: "BeforePhotosCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BeforePhotosCollectionCell")
        addSuppliesTextField.delegate = self
        addSuppliesTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: .editingChanged)
        
        
    }
    
    
    //MARK: MANAGE TABLE VIEW HEIGHT DYNAMICALLY
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        supplyDisplayTableViewHeight.constant = supplyDisplayTableView.contentSize.height
        super.updateViewConstraints()
    }
    
    //MARK: IB_ACTIONS
    @IBAction func addTaskBtnTapped(_ sender: UIButton) {
        guard let listTaskVc = storyboard?.instantiateViewController(withIdentifier: "ListTaskVC") as? ListTaskVC else {return}
        navigationController?.pushViewController(listTaskVc, animated: true)
    }

}

//MARK: Text Field Delegate Implementation
extension AddTaskVC: UITextFieldDelegate {
    
    @objc
    func textFieldEditingDidChange(_ textField: UITextField) {
        
        switch textField {
        case addSuppliesTextField:
            //unhide the supply search table view
            guard let text = textField.text else {return}
            supplySearchTableView.isHidden = text.isEmpty ? true : false
        default:
            return
        }
        
    }
    
}

//MARK: TableView Delegate & Data Source Implementation
extension AddTaskVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case supplyDisplayTableView:
            return 4
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case supplyDisplayTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SupplyDisplayTblCell") as? SupplyDisplayTblCell else {return UITableViewCell()}
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: BEFORE PHOTOS COLLECTION VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension AddTaskVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BeforePhotosCollectionCell", for: indexPath) as? BeforePhotosCollectionCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - 10)/2
        let cellHeight = collectionView.bounds.height
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}
