//
//  AddEstimateVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 25/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ListEstimateVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var emptyContentStackView: UIStackView!
    @IBOutlet weak var estimateTableView: UITableView!
    
    //Variables
    var listEstimatesDataArr = [ListEstimateDetailsData]()
    
    //Pagination Variables
    var currentPageNumber = 1
    var maxPageNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        title = "Estimates"
        estimateTableView.register(UINib(nibName: "EstimationDetailTblCell", bundle: nil), forCellReuseIdentifier: "EstimationDetailTblCell")
        estimateTableView.register(UINib(nibName: "EstimationHeaderTblCell", bundle: nil), forCellReuseIdentifier: "EstimationHeaderTblCell")
        estimateTableView.delegate = self
        estimateTableView.dataSource = self
        emptyContentStackView.isHidden = true
        serviceCalls()
    }
    
    fileprivate func serviceCalls() {
        getEstimateList(pageNumber: 1)
    }
    
    //IB_ACTIONS
    @IBAction func addEstimateBtnTapped(_ sender: UIButton)  {
        //Go to create new estimate view controller
        guard let createNewEstimateVc = storyboard?.instantiateViewController(withIdentifier: "CreateNewEstimateVC") as? CreateNewEstimateVC else {return}
        navigationController?.pushViewController(createNewEstimateVc, animated: true)
    }
    
}

extension ListEstimateVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listEstimatesDataArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listEstimatesDataArr[section].estimates.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EstimationHeaderTblCell") as! EstimationHeaderTblCell
        cell.configureCell(estimateDate: listEstimatesDataArr[section])
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EstimationDetailTblCell") as? EstimationDetailTblCell else {return UITableViewCell()}
        let estimateData = listEstimatesDataArr[indexPath.section].estimates[indexPath.row]
        cell.configureCell(estimateDetail: estimateData, delegate: self, indexpath: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == listEstimatesDataArr.count - 1 && currentPageNumber < maxPageNumber {
            
            currentPageNumber += 1
            batchFetchEstimateList(pageNumber: currentPageNumber)
            //print("It is the end of TableView")
        }
        
    }
    
}

extension ListEstimateVC: EstimationDetailTblCellDelegate {
    
    func didTapEstimateEditBtn(estimateDetail: Estimate, at indexpath: IndexPath) {
        guard let createNewEstimatevc = storyboard?.instantiateViewController(withIdentifier: "CreateNewEstimateVC") as? CreateNewEstimateVC else {return}
        createNewEstimatevc.isInvoiceEdit = true
        createNewEstimatevc.estimateId = estimateDetail.id
        navigationController?.pushViewController(createNewEstimatevc, animated: true)
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension ListEstimateVC {
    
    //MARK: GET ESTIMATE LIST SERVICE
    
    //Todo: Pagination Part Pending here.
    
    func getEstimateList(pageNumber: Int) {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters = ["page_number": pageNumber]
        
        self.view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [unowned self] (listEstimates: ListEstimate!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listEstimates {
                    if returnedResponse.success == "true" {
                        //success
                        
                        if let data = returnedResponse.data {
                            self.listEstimatesDataArr = data.estimatesDetailData
                            //self.listEstimatesDataArr.append(contentsOf: data.estimatesDetailData)
                            self.maxPageNumber = data.pageCount
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.estimateTableView.reloadData()
                        }
                        
                        //print(self.listEstimatesDataArr)
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
}

//MARK: PAGINATION API SERVICE CALLS
extension ListEstimateVC {
    
    func batchFetchEstimateList(pageNumber: Int) {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: token)!)"
        ]
        
        let parameters = ["page_number": pageNumber]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (listEstimates: ListEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
                
            }
            else {
                
                if let returnedResponse = listEstimates {
                    
                    if returnedResponse.success == "true" {
                        
                        //Success
                        if let data = returnedResponse.data {
                            
                            self.listEstimatesDataArr.append(contentsOf: data.estimatesDetailData)
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.estimateTableView.reloadData()
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
