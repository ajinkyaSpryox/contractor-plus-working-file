//
//  CreateNewEstimateVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 26/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import SwiftyJSON
import SVProgressHUD

class CreateNewEstimateVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var selectClientTextField: UITextField!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var transactionTypeLbl: UILabel!
    
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var stateNameTextField: UITextField!
    @IBOutlet weak var cityNameTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!
    
    @IBOutlet weak var labourRateTextField: UITextField!
    @IBOutlet weak var supplyRateTextField: UITextField!
    
    //Received Variables
    var isInvoiceEdit = false
    var estimateId = 0
    
    //Variables
    var clientPickerData = [ClientEstimateDetail]()
    var selectedClientId = 0
    var selectedClientAddressId = 0
    var selectedSegmentUnitType = "value"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        title = "New Estimate"
        selectClientTextField.delegate = self
        segmentController.selectedSegmentIndex = 1
        serviceCalls()
        
    }
    
    fileprivate func serviceCalls() {
        getClientsListForEstimate()
        
        if isInvoiceEdit {
            getEstimateDetailWith(estimateId: estimateId)
        }
        
    }
    
    //IB-ACTIONS Implementation
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        //Go to Save Estimate View Controller
        createNewEstimate()
        
    }
    
    
    @IBAction func segmentControlChanged(_ sender: UISegmentedControl) {
           
           selectedSegmentUnitType =  sender.selectedSegmentIndex == 0 ? "percent" : "value"
           transactionTypeLbl.text = sender.selectedSegmentIndex == 0 ? "%" : "$"
           
    }
    
}

//MARK: TextField Delegate Implementation
extension CreateNewEstimateVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //if text field is client then add picker view
        if textField == selectClientTextField {
            addPickerToClientTextField()
        }
        else {
            return
        }
    }
    
    func addPickerToClientTextField() {
        let clientIdPicker = UIPickerView()
        selectClientTextField.inputView = clientIdPicker
        clientIdPicker.delegate = self
        clientIdPicker.dataSource = self
    }
    
}

//MARK: UIPICKER VIEW DELEGATE IMPLEMENTATION
extension CreateNewEstimateVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return clientPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return clientPickerData[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectClientTextField.text = clientPickerData[row].name
        self.selectedClientId = clientPickerData[row].id
        
        //Show Address Table Popup
        if self.selectedClientId > 0 {
            
            pickerView.resignFirstResponder()
            selectClientTextField.resignFirstResponder()
            
            guard let addressPopvc = self.storyboard?.instantiateViewController(withIdentifier: "AddressPopupVC") as? AddressPopupVC else {return}
            addressPopvc.clientId = self.selectedClientId
            addressPopvc.delegate = self
            addressPopvc.modalTransitionStyle = .crossDissolve
            addressPopvc.modalPresentationStyle = .overCurrentContext
            self.present(addressPopvc, animated: true, completion: nil)
        }
        else {
            self.view.makeToast("Please Selected Client", duration: 2.0, position: .bottom)
        }
        
    }
    
}

//MARK: ADDRESS POPUP View Controller Delegate Implementation
extension CreateNewEstimateVC: AddressPopupVCDelegate {
    
    func userDidSelectAddress(selectedAddress: AddressDetail) {
        address1TextField.text = selectedAddress.address1
        address2TextField.text = selectedAddress.address2
        stateNameTextField.text = selectedAddress.state
        cityNameTextField.text = selectedAddress.city
        zipcodeTextField.text = selectedAddress.zipcode
        self.selectedClientAddressId = selectedAddress.addressID
        
    }
    
}

//MARK: API SERVICE IMPLEMENTATIONS
extension CreateNewEstimateVC {
    
    //Todo: Get Clients Name in Drop Down
    func getClientsListForEstimate() {
        
        let params = ["page_number": 0]
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_client_for_estimate, method: .post, parameters: params, encodingType: URLEncoding.default, headers: headers) { [weak self] (clientList: EstimateClientsList!, errorMessage) in
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = clientList {
                    if returnedResponse.success == "true" {
                        //Success
                        
                        returnedResponse.data.clients.forEach { (client) in
                            client.clientEstimateDetails.forEach { (detail) in
                                self.clientPickerData.append(detail)
                            }
                        }
                        
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            
                        }
                    }
                }
                
            }
            
            
        }
        
    }
    
    //Todo: Create New Estimate
    func createNewEstimate() {
        
        guard let supplyRate = supplyRateTextField.text, let labourRate = labourRateTextField.text else {return}
        
        let parameters: [String:Any] = ["estimate_id": "", "user_id": "", "client_id": self.selectedClientId, "client_address_id": selectedClientAddressId, "labour_rate": labourRate, "supply_markup_value": supplyRate, "supply_markup_unit": selectedSegmentUnitType, "comment": ""]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        API.callAPI(Webservices.create_estimate, headers: headers, params: parameters) { (response, errorStr) in
            
            guard errorStr == nil else {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                //print(errorStr!)
                return
            }
            
            if let json = response as? JSON {
                
                SVProgressHUD.dismiss()
                let Success = json["success"].stringValue
                let Message = json["message"].stringValue
                
                
                if(Success == "true"){
                    //Success
                    self.view.makeToast(Message, duration: 2.0, position: .center)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        guard let saveEstimateVc = self.storyboard?.instantiateViewController(withIdentifier: "SaveEstimationVC") as? SaveEstimationVC else {return}
                        self.navigationController?.pushViewController(saveEstimateVc, animated: true)
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast(Message, duration: 2.0, position: .center)
                    }
                }
                
            }
        }
        
    }
    
    //Todo: Edit Previous Estimate
    func getEstimateDetailWith(estimateId: Int) {
        
        view.makeToastActivity(.center)
        
        let parameters = ["estimate_id": estimateId]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.get_estimate_details, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (estimateDetails: EstimateDetail!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = estimateDetails {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        print(returnedResponse.data)
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
}
