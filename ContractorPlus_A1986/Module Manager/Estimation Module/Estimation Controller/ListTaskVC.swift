//
//  ListTaskVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class ListTaskVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var listTaskTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        title = "List Task"
        listTaskTableView.delegate = self
        listTaskTableView.dataSource = self
        listTaskTableView.register(UINib(nibName: "ListTaskTblCell", bundle: nil), forCellReuseIdentifier: "ListTaskTblCell")
    }
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListTaskVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTaskTblCell") as? ListTaskTblCell else {return UITableViewCell()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
