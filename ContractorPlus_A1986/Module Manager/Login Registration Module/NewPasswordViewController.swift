//
//  NewPasswordViewController.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 11/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//
import UIKit
import SwiftyJSON
import SVProgressHUD
import SwiftHash
import Alamofire
import Toast_Swift

class NewPasswordViewController: UIViewController {
    
    //Variables
    var isFromSocialLogin = false
    
    
    @IBOutlet weak var otpNoTF: UITextField!
    
    @IBOutlet weak var newPasswordTF: UITextField!
    
    @IBOutlet weak var sendOTPBtn: UIButton!
    
    @IBAction func resendBtnAction(_ sender: UIButton) {
        
        api_Resend_Function()
        
    }
    
    
    
    @IBAction func otpBtnAction(_ sender: UIButton) {
        
        switch isFromSocialLogin {
        case true:
            
            hideNewPassword()
            if otpNoTF.text != "" {
                //Call verify Service
                verifyOtp()
            }
            
        default:
            if otpNoTF.text! != "" && newPasswordTF.text! == "" {
                hideOTP()
                showNewPassword()
            }else if newPasswordTF.text! != ""{
                
                apiFunction()
                
            }else{
                
                self.errorAlert(message: textfieldEmptyError)
            }
        }
        
        
        
        
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBOutlet weak var OTPConstraint: NSLayoutConstraint!
    @IBOutlet weak var OTPViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var newPasswordConstraint: NSLayoutConstraint!
    @IBOutlet weak var newPasswordViewConstraint: NSLayoutConstraint!
    
    
    func hideNewPassword(){
        newPasswordConstraint.constant = 0
        newPasswordViewConstraint.constant = 0
        newPasswordTF.alpha = 0
        
    }
    
    func showNewPassword(){
        
        newPasswordConstraint.constant = 25
        newPasswordViewConstraint.constant = 60
        newPasswordTF.alpha = 1
    }
    func hideOTP(){
        
        OTPConstraint.constant = 0
        OTPViewConstraint.constant = 0
        otpNoTF.alpha = 0
        
    }
    
    func ShowOTP(){
        OTPConstraint.constant = 25
        OTPViewConstraint.constant = 60
        otpNoTF.alpha = 1
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideNewPassword()
        
        sendOTPBtn.cornerRadius1(value: 12.0, shadowColor: UIColor.red.cgColor, borderColor: UIColor.clear.cgColor)
        otpNoTF.addBottomBorderWithColor(color: UIColor.red, width: 3)
        newPasswordTF.addBottomBorderWithColor(color: UIColor.red, width: 3)
        
        
        // Do any additional setup after loading the view.
    }
    func apiFunction(){
        
        let header = ["Accept" : "application/json",
                      "x-api-key": appXApiKey]
        
        let parameter : [String : Any] = [
            
            "user_id" :  "\(UserDefaults.standard.string(forKey: user_id)!)",
            "otp" : otpNoTF.text!,
            "password" : MD5(self.newPasswordTF.text!).lowercased()
            
            
        ]
        print(parameter)
        
        API.callAPI(Webservices.forget_password_verify_otp, headers: header, params: parameter) { (response, errorStr) in
            
            guard errorStr == nil else {
                SVProgressHUD.dismiss()
                self.errorAlert(message: errorStr!)
                return
            }
            
            if let json = response as? JSON {
                
                print(json)
                SVProgressHUD.dismiss()
                
                //                 let json = try JSON(data: newData)
                let postdata = json["data"]
                let Success = json["success"].boolValue
                let Message = json["message"].stringValue
                let eventsData = json["data"]["events"]
                
                
                if(Success == true){
                    UserDefaults.standard.set(postdata["token"].stringValue, forKey: token )
                    //                        self.successAlert(AppName, message: Message) {
                    
                    //                            self.performSegue(withIdentifier: "MenuVC", sender: nil)
                    DispatchQueue.main.async {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    //                        }
                }else{
                    self.errorAlert(message: Message)
                }
                
            }
        }
        
    }
    
    func userAuthenticated(){
        
        var window: UIWindow?
        let navController = UIStoryboard(name: "LOGIN", bundle: nil).instantiateViewController(withIdentifier: "MenuVC")
        //let navController = UINavigationController(rootViewController: VC1)
        //navController.isNavigationBarHidden = true
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        
    }
    
    func api_Resend_Function(){
        
        let header = ["Accept" : "application/json",
                      "x-api-key": appXApiKey]
        
        let parameter : [String : Any] = [
            
            //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
            //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
            
            "mobile_no" :  "\(UserDefaults.standard.string(forKey: mobile_no)!)",
            "isd_code" : "\(UserDefaults.standard.string(forKey: isd_code)!)"
            
            
        ]
        
        
        API.callAPI(Webservices.send_otp, headers: header, params: parameter) { (response, errorStr) in
            
            guard errorStr == nil else {
                SVProgressHUD.dismiss()
                self.errorAlert(message: errorStr!)
                return
            }
            
            if let json = response as? JSON {
                
                print(json)
                SVProgressHUD.dismiss()
                
                //                 let json = try JSON(data: newData)
                let postdata = json["data"]
                let Success = json["success"].boolValue
                let Message = json["message"].stringValue
                let eventsData = json["data"]["events"]
                
                
                if(Success == true){
                    self.successAlert(message: Message)
                }else{
                    self.errorAlert(message: Message)
                }
                
            }
        }
        
    }
    
    func verifyOtp() {
        
        view.makeToastActivity(.center)
        
        
        let parameter : [String : Any] = [
            "user_id" :  "\(UserDefaults.standard.string(forKey: user_id)!)",
            "otp" : otpNoTF.text!,
        ]
        
        let headers = ["Content-Type": "application/json", "x-api-key": appXApiKey]
        
        GenericWebservice.instance.getServiceData(url: Webservices.verify_otp, method: .post, parameters: parameter, encodingType: JSONEncoding.default, headers: headers) { [weak self] (returnedUserCredentials: SocialLoginDetail!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = returnedUserCredentials {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        //print(returnedResponse.data)
                        UserDefaults.standard.set(returnedResponse.data.token, forKey: token)
                        UserDefaults.standard.set(returnedResponse.data.role, forKey: role)
                        UserDefaults.standard.set(returnedResponse.data.isOtpVerified, forKey: is_otp_verified)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.firstName, forKey: first_name)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.lastName, forKey: last_name)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.email, forKey: email)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.fullName, forKey: full_name)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.id, forKey: user_id)
                        UserDefaults.standard.set(true, forKey: "is_authenticated")
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            //Navigate to Main Dashboard
                            let storyBoard : UIStoryboard = UIStoryboard(name: "LOGIN", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MenuVC") as! UINavigationController
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            appdelegate.window!.rootViewController = nextViewController
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
            }
            
        }
        
    }
    
}
