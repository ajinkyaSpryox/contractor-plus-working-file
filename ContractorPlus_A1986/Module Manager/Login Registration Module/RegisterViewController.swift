
import UIKit
import SwiftyJSON
import SVProgressHUD
//import NKVPhonePicker
import CountryList
import SwiftHash
class RegisterViewController: UIViewController ,CountryListDelegate{

    @IBOutlet weak var logoImgView: UIImageView!
    
    @IBOutlet weak var lastnameTF: UITextField!
    @IBOutlet weak var firstnametF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var mobileNoTF: UITextField!
    
    @IBOutlet weak var registerBtn: UIButton!
    
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func regsiterBtnAction(_ sender: UIButton) {
    
//        MD5("https://www.google.com")
        print(MD5("12345678").lowercased())
//         self.errorAlert(message: textfieldEmptyError)
        
         if firstnametF.text!.isEmpty == false && firstnametF.text! != ""{

           if lastnameTF.text!.isEmpty == false && lastnameTF.text! != "" {

             if codeTF.text!.isEmpty == false && codeTF.text! != "" {

                if mobileNoTF.text!.isEmpty == false && mobileNoTF.text! != "" {

                    if emailTF.text!.isEmpty == false && emailTF.text! != "" {

                        if passwordTF.text!.isEmpty == false && passwordTF.text! != ""{


                            apiFunction()


                         }else {
                            self.errorAlert(message: textfieldEmptyError)
                            }
                        }else {
                    self.errorAlert(message: textfieldEmptyError)
                   }
                }else {
                 self.errorAlert(message: textfieldEmptyError)
               }
              }else {
              self.errorAlert(message: textfieldEmptyError)
             }
            }else {
            self.errorAlert(message: textfieldEmptyError)
            }
            }else {
            self.errorAlert(message: textfieldEmptyError)
            }
            
    }
    
    @IBAction func selecteCountryBtnAction(_ sender: UIButton) {
    
           let navController = UINavigationController(rootViewController: countryList)
             self.present(navController, animated: true, completion: nil)
    
    }
    var countryList = CountryList()
    
    @IBOutlet weak var firstnameUiView: UIView!
    @IBOutlet weak var lastnameUiView: UIView!
    
    @IBOutlet weak var passwordUiview: UIView!
    @IBOutlet weak var emailUiView: UIView!
    
    @IBOutlet weak var mobileNo: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countryList.delegate = self
        mobileNo.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        firstnameUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        lastnameUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        passwordUiview.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        emailUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        registerBtn.cornerRadius1(value: 12.0, shadowColor: UIColor.red.cgColor, borderColor: UIColor.clear.cgColor)
       
        // Do any additional setup after loading the view.
         
    }
   
    func selectedCountry(country: Country) {
            
        codeTF.text = "+\(country.phoneExtension)"
    }
    
    func apiFunction(){
        
        let header = ["Accept" : "application/json",
                      "x-api-key": appXApiKey]
        
//        {
//            "first_name" : "Akash",
//            "last_name" : "Parmar",
//            "email" : "akashp.spryox@gmail.com",
//            "isd_code" : "+91",
//            "mobile_no" : "9167023220",
//            "password" : "12345678",
//            "fcm_token" : "asfnoluihasf",
//            "role_id" : "1"
//        }
        
        
        let parameter : [String : Any] = [
                       
//                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
//                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
            
                              "first_name" : self.firstnametF.text!,
                              "last_name" : self.lastnameTF.text!,
                              "email" : self.emailTF.text!,
                              "isd_code" : self.codeTF.text!,
                              "mobile_no" : self.mobileNoTF.text!,
                              "password" : MD5(self.passwordTF.text!).lowercased(),
                              "fcm_token" : "asfnoluihasf",
                              "role_id" : "contractor"
            
                   
        ]
        
        
        API.callAPI(Webservices.register, headers: header, params: parameter) { (response, errorStr) in
            
            guard errorStr == nil else {
                SVProgressHUD.dismiss()
                self.errorAlert(message: errorStr!)
                return
            }
            
            if let json = response as? JSON {
                
                print(json)
                SVProgressHUD.dismiss()
               
//                 let json = try JSON(data: newData)
                   let postdata = json["data"]
                   let Success = json["success"].boolValue
                   let Message = json["message"].stringValue
                   let eventsData = json["data"]["events"]
                  
                
                   if(Success == true){
                    self.successAlert(AppName, message: Message) {
                       
                        UserDefaults.standard.set(self.codeTF.text!, forKey: isd_code )
                        UserDefaults.standard.set(self.mobileNoTF.text!, forKey: mobile_no )
                        UserDefaults.standard.set(postdata["user_details"]["id"].stringValue, forKey: user_id)
                        
                        self.performSegue(withIdentifier: "otpVC", sender: nil)
                    }
                   }else{
                    self.errorAlert(message: Message)
                }
                
            }
        }
        
    }
    

}
