

import UIKit
import SideMenu

class MainHomeScreenViewController: UIViewController , UITableViewDelegate , UITableViewDataSource,UICollectionViewDelegate, UICollectionViewDataSource{
    
    

    
    @IBOutlet weak var notificationBarBtn: UIBarButtonItem!
    
    
    @IBOutlet weak var menuBarBtn: UIBarButtonItem!
    
    
    
    @IBAction func MenuBarBtnAction(_ sender: UIBarButtonItem) {
    }
    
    
    @IBAction func NotificationBarBtnAction(_ sender: UIBarButtonItem) {
    }
    

    @IBOutlet weak var homeCLC_View: UICollectionView!
    
    @IBOutlet weak var homeTblView: UITableView!
    
    var tblViewNameList = [String]()
    var tblViewImgList = [String]()
    var clcViewImgList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        let titleTextAttributed: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white, .font: UIFont(name: "NotoSans-Regular", size: 18) as Any]
        navigationController?.navigationBar.titleTextAttributes = titleTextAttributed
        
        
        tblViewNameList = ["Estimates","Invoices","Post Inspections","Shopping List","Schedule","Team Member","Clients","Mileage Log","Reports","Settings","Time Clock"]
        
        tblViewImgList = ["Estimates_home.png","Invoices_home.png","Post-Inspections_home.png","Shopping-List_home.png","Schedule_home.png","Team-Member_home.png","Clients_home.png","Mileage-Log_home.png","Reports_home.png","settings_home.png","Time-Clock_home.png"]
        
        clcViewImgList = ["01_home.png","02_home.png","03_home.png","04_home.png"]
        
          SideMenuManager.default.menuPresentMode = SideMenuManager.MenuPresentMode.menuSlideIn
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblViewNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeScreenListTableViewCell
        
        cell.listImgView.image = UIImage(named: tblViewImgList[indexPath.row])
        cell.listNameLbl.text = tblViewNameList[indexPath.row]
        cell.mainUiView.cornerRadius1(value: 8.0, shadowColor: UIColor.lightGray.cgColor, borderColor: UIColor.clear.cgColor)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clcViewImgList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! HomeScreenListCollectionViewCell
        cell.listImgView.image = UIImage(named: clcViewImgList[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0:
            //Go to estimate storyboard
            guard let addEstimateVc = STORYBOARDNAMES.ESTIMATES_STORYBOARD.instantiateViewController(withIdentifier: "ListEstimateVC") as? ListEstimateVC else {return}
            navigationController?.pushViewController(addEstimateVc, animated: true)
            
        case 1:
            //Go to Invoices
            guard let manageInvoicesVc = STORYBOARDNAMES.INVOICES_STORYBOARD.instantiateViewController(withIdentifier: "ManageInvoicesVC") as? ManageInvoicesVC else {return}
            navigationController?.pushViewController(manageInvoicesVc, animated: true)
            
        case 6:
            guard let clientsVc = STORYBOARDNAMES.CLIENTS_STORYBOARD.instantiateViewController(withIdentifier: "ClientListViewController") as? ClientListViewController else {return}
            navigationController?.pushViewController(clientsVc, animated: true)
            
        case 9:
            //Go to Main Settings List
            guard let mainSettingsListVc = STORYBOARDNAMES.SETTINGS_STORYBOARD.instantiateViewController(withIdentifier: "MainSettingsListVC") as? MainSettingsListVC else {return}
            navigationController?.pushViewController(mainSettingsListVc, animated: true)
            
            
        default:
            return
        }
    }
}
