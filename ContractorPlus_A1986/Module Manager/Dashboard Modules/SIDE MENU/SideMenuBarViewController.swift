
import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn

class SideMenuBarViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{

    
    @IBOutlet weak var sideMenuTblView: UITableView!
    
    var listnameArray = [String]()
    var listslugArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listnameArray = ["My Account","Contractor Plus PRO","Help","About","Rate the App","Share App","Logout"]
        
        listslugArray = ["My-account.png","Contractor-Plus-PRO.png","Help.png","About.png","Rate-the-App.png","Share--App.png","Logout.png"]
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listnameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! sideMenuListTableViewCell
        cell.iconImgView.image = UIImage(named: listslugArray[indexPath.row])
        
        cell.listnameLbl.text = listnameArray[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! sideMenuHeaderTableViewCell
//        cell.profilePicImgView.image = 
//        cell.usernameLbl.text  =
//        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 6:
            
            if AccessToken.current != nil {
                //Logout from facebook
                print("Facebook Logout")
                let loginManager = LoginManager()
                loginManager.logOut()
                logoutUser()
            }
            else if (GIDSignIn.sharedInstance()?.currentUser != nil) {
                //Logout from Google
                print("Google Logout")
                GIDSignIn.sharedInstance()?.signOut()
                logoutUser()
            }
            else {
                //Normal Logout
                logoutUser()
            }
            
            
        default:
            return
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
}
