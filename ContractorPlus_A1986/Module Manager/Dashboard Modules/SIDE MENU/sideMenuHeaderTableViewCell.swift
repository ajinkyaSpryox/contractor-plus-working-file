
import UIKit

class sideMenuHeaderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var usernameLbl: UILabel!
    
    @IBOutlet weak var profilePicImgView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
